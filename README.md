# Samply Auth Client

The Samply Auth Client library offers a client, that uses the REST interface
of the Samply Auth application. It offers methods for all Samply Auth workflows:

- Your application is a registered client in the Samply Auth service
- Your application is *not* a registered client in the Samply Auth service
- Your application already has an access token whose validity is nearing its end

An access token is valid for several hours. After this period of time, the
access token is no longer valid and should be exchanged for a new access token,
if necessary. You can use the refresh token that you got earlier to get a new
access token. This approach will only work if you used the first approach
earlier.

As of May 31st, 2017, the repository has moved to https://bitbucket.org/medicalinformatics/samply.auth.client. Please see the following help article on how to change the repository location in your working copies:

* https://help.github.com/articles/changing-a-remote-s-url/

If you have forked Mainzelliste-OSSE in Bitbucket, the fork is now linked to the new location automatically. Still, you should change the location in your local copies (usually, the origin of the fork is configured as a remote with name "upstream" when cloning from Bitbucket).